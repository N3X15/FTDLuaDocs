# About

This is some documentation for a Lua mod I worked on prior to signing on with Brilliant Skies.

This is now probably out of date.

# ETA
SOON[tm].  (After the modding framework is in.)

# Feature Sets

**Note:** LuaBinding is the I passed to Update(I).

## Fleet Awareness

### LuaBinding
|   |   |
|---|---|
| int I.FleetIndex  | Index of the craft within the fleet. Starts at 0. |
| FleetInfo I.Fleet | Information about the craft's current fleet.      |
| bool I.IsFlagship | Is the ship the Flagship of the fleet?            |

### FleetInfo
|   |   |
|---|---|
| int FI.ID                     | ID number of the Fleet.                           |
| string FI.Name                | Name of the Fleet.                                |
| FriendlyInfo FI.Flagship      | Information regarding the flagship of the fleet.  |
| List&lt;FriendlyInfo&gt; FI.Members | A list of fleet members. MAY CONTAIN NILS!        |

### FriendlyInfo
|   |   |
|---|---|
| int FI.FleetID                | ID number of the Fleet this friendly is in.       |

## Resource Awareness

### LuaBinding
|   |   |
|---|---|
| List&lt;ResourceZoneInfo&gt; I.ResourceZones  | List of resource zones.            |
| ResourceInfo rzi.Resources | Raw resources available in this construct.         |

### ResourceZoneInfo
|   |   |
|---|---|
| int rzi.ID               | ID number of the zone.                        |
| string rzi.Name          | Name of the zone.                             |
| float rzi.Radius         | Radius (m) of the zone.                       |
| Vector3 rzi.Position     | Global position of the zone.                  |
| ResourceInfo rzi.Resources | Raw resources available in this zone.        |

### ResourceInfo
|   |   |
|---|---|
| float rzi.CrystalTotal   | Available Crystal in this zone.               |
| float rzi.CrystalMax     | Maximum amount of Crystal this zone can hold. |
| float rzi.MetalTotal     | Available Metal in this zone.                 |
| float rzi.MetalMax       | Maximum amount of Metal this zone can hold.   |
| float rzi.NaturalTotal   | Available Natural in this zone.               |
| float rzi.NaturalMax     | Maximum amount of Natural this zone can hold. |
| float rzi.OilTotal       | Available Oil in this zone.                   |
| float rzi.OilMax         | Maximum amount of Oil this zone can hold.     |
| float rzi.ScrapTotal     | Available Scrap in this zone.                 |
| float rzi.ScrapMax       | Maximum amount of Scrap this zone can hold.   |

## Fortress Stuff

### LuaBinding
|   |   |
|---|---|
| void I:MoveFortress(Vector3 direction) | Move fortress 1m in the given direction maximum. |


## Waypoints

### LuaBinding
|   |   |
|---|---|
| Vector3 I.Waypoint              | Current waypoint for this force. (read only)             |
| Vector3 I.IdealFleetPosition    | Current ideal fleet position for this force relative to flagship. (set & get) |
| Quaternion I.IdealFleetRotation | Current ideal fleet rotation for this force relative to flagship. (set & get) |

## Command Interface

### LuaBinding
|   |   |
|---|---|
| string I.AIMode        | Current AI mode for this force. (read only) Can be `on`, `off`, `combat`, `fleetmove`, or `patrol`. Reads off first mainframe. |
| string I.ConstructType | Gets or sets the type of construct (requires mainframe). Can be `none`, `naval`, `aerial`, or `fortress`. Affects command buttons. |

## Missile Messaging Interface
**Note:** I don't like this very much, but will have to do for now until an internal refactor can be done.

### LuaBinding
|   |   |
|---|---|
| MissileInfo I.GetMissileInfo(int luaTransceiverIndex, int missileIndex) | Gets handle for the given missile. CAN RETURN NIL.|

### MissileInfo
|   |   |
|---|---|
| List&lt;MissilePartInfo&gt; MI.Parts | Gets information about each block of the missile. |

### MissilePartInfo
|   |   |
|---|---|
| string MPI.Name | Gets the name of the missile part. |
| List&lt;float&gt; MPI.Registers | Gets current state of the part's memory registers, which store configuration information. |
| void MPI:SendRegister(int regIndex, float value) | Set the value of the given register. Each missile part has 2 registers. **Note:** For legal reasons, I cannot document what each part's registers are.  Experiment. |
